<?php
/**
 * The plugin that handles a full page.
 *
 * @ingroup views_display_plugins
 */
class views_page_disable_breadcrumb_views_plugin_display_page extends views_plugin_display_page
{
    function uses_breadcrumb() { return false; }
}